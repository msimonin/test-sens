---
title: communication/orga du groupe
scope: private
---

- Agenda: [ICS](https://zimbra.inria.fr/home/matthieu.simonin@inria.fr/SEnS.ics) — [Affichage](https://zimbra.inria.fr/home/matthieu.simonin@inria.fr/SEnS.html) — [Outlook](webcals://zimbra.inria.fr/home/matthieu.simonin@inria.fr/SEnS)
  - Les accès en écriture sont données aux personnes qui le demandent gentillement

- [Mattermost](https://mattermost.inria.fr/sens/channels/groupe-sens-de-rennes)

- [Mailing list](https://sympa.inria.fr/sympa/info/sens-rennes) sens-rennes@inria.fr
- [Groupe Gitlab](https://gitlab.inria.fr/sens-rennes)
    - groupe privé pour le moment

# Organisation d'une journée Sens

## Quelques semaines avant

- Evento
- Mail d'annonce (remplacer DATE et EVENTO_URL)

```

Sujet: Atelier Sciences, Environnements et Sociétés le {{ DATE }}
---

Bonjour,

Nous organisons un nouvel atelier SEnS le {{ DATE }} (comme le
précédent, toute la journée). 

Inscription

Inscription pour la journée complète (9h-17h30) :

{{ EVENTO_URL }}

Si vous êtes intéressés mais pas disponibles (ou qu'il ne reste plus de place),
cochez la seconde option. Si la demande est suffisante, nous organiserons un
autre prochainement.


Objectif

Cet atelier est conçu dans le but de fournir des outils et des ressources pour
approfondir la question des conséquences de nos recherches, comprendre avec
quelles valeurs les sciences sont produites, et construire un espace commun pour
se positionner sur les enjeux environnementaux, au-delà des calculs d’empreinte
environnementale. Il a été conçu par et pour des chercheurs et chercheuses en
sciences “dures” mais il est ouvert à tous les personnes qui travaillent dans
l’enseignement supérieur et la recherche (par exemple les services de médiation,
de transfert technologique, etc.). Il ne vise pas à mettre tout le monde
d’accord, mais à mettre à plat les désaccords pour construire un “commun
scientifique”.



Contexte

Cet atelier est une réaction aux ravages écologiques en cours, les scénarios
catastrophe du GIEC, la nécessité de revoir nos modes de production et de
consommation qui est avancée par la communauté scientifique elle-même rendent la
question inévitable pour toutes et tous, à tous les niveaux d’organisation :
qu’en est-il de notre activité de recherche scientifique ? Comment
s’intègre-t-elle dans les sociétés actuelles et dans la part destructrice de
leur fonctionnement et comment l’intégrer dans les scénarios prospectifs à moyen
et long termes ? Cette question en ouvre une plus large, plus ancienne, mais
dont les scientifiques s’emparent rarement : est-ce que les résultats de nos
recherches participent à la construction d’un monde qui correspond à nos valeurs
? Pour aborder cette question, il faut apprendre à expliciter les valeurs avec
lesquelles nous travaillons, et comprendre les tenants et aboutissants de nos
recherches, démarche qui relève de l’histoire, de l’économie, du droit, de la
philosophie, de la sociologie, de la politique, et de l’éthique de nos
disciplines, auxquels nous ne sommes, en majorité, pas formés.  

```

- Trouver une salle (par exemple Pétri Turing nécessite de passer par un.e assistant.e)

- Ajouter dans l'agenda partagé

- Envoyer le mail d'annonce à `tous-rba-irisa@inria.fr`

- Réclamer un coup de main pour l'animation sur nos canaux (mailing list / mattermost)

- Mettre une annonce dans l'intranet (exemple: https://intranet.inria.fr/Actualite/Atelier-Sciences-Environnements-et-Societes-SeNS)
  Au plus tard le vendredi précédent pour apparaître dans l'hebdo Inria.


## Une semaine avant

- Mail de rappel avec salle et infos pratique
```
Bonjour,

Merci de vous être inscrits à l'atelier de {{ DATE }} !

Quelques infos pratiques:

- L'atelier se déroulera en salle  {{ ROOM }}

- L'atelier est composé de deux sessions: 9h-12h45 et 14h-17h.

- Vous êtes libres pour le déjeuner. Ca peut être l'occasion de faire
une vraie pause, mais pour ceux qui le souhaitent, on peut manger
ensemble à la caféteria.

- Il y a deux pauses de prévues de 15minutes, une le matin et une l'après-midi.

- A prendre: un stylo, un peu de papier

- **A ne pas prendre: un ordinateur**

A bientôt,
```
