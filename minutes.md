---
title: Meeting minutes
---

## Réunion 2022-10-24
- Organisation du 4 novembre : Thomas, Simon, Davide, Caroline
- Prochains ateliers : 16 janvier, 9 février
- réunion du 25 novembre
- site web ?

## Réunion 2022-10-03
- Organisateur du 12: Simon, Martin et Caroline qui écoute
- Nouvel atelier: 4 Novembre
- Faire des ateliers en dehors du labo ?

## Réunion 2022-09-16

*Ordre du jour*.
- Fixer prochaine réunion
- Organisation d'atelier SeNS au labo.
- Dates pour le second atelier:
  *4 Novembre*: Thomas, Caroline, Simon, Davide
  - [ ] Relancer les gens qui n'étaient pas dispo
  - [ ] Réserver la salle

*A faire pour le 12*.
- Préparer l'intro: effet rebond, on optimise tous et pourtant rien ne change / s'empire

Organisation de l'atelier du 12: [Déroulé de l'atelier](https://sens-gra.gitlabpages.inria.fr/atelier-impacts-recherche/documents/deroule.pdf)

#### Préparation d'ateliers SeNS à l'Automne
Matériels:
- [ ] Imprimer des exemplaires du manuel
- [ ] Post-It

Préparation:
- [ ] Lecture de la ressource documentaire
- [ ] Réunion de préparation
- [x] Réserver les salles
- [x] Créer l'event
- [x] Envoyer un email à tout le monde.

- Deux ateliers à l'automne.
  - Mercredi 12 octobre (9h-17h) (*Animateurs.* Simon, Martin, Davide, Caroline). 
  - Lundi 14 novembre (9h-17h) (*Animateurs*. Simon, Martin, Thomas (?), Anne-Cécile (?), Davide (?))

#### Prochaine réunion post-SeNS
Réunion après le premier atelier.

Que faire:
- Thomas B. nous parle de fresque du climat
- Groupe de lecture (Lowtech magazine, philo/socio)
- Quelqu'un parle 15minutes sur un truc argumenté (billet de blog, lecture, ...) (Thomas M.: Science & démocratie)
- Réunion fin novembre/déc après les deux ateliers
  *25 novembre à 10-12h*
  
### Réunion 2022-07-20

- Tour de table
    - Présents: Simon Castellan, Thomas Maugey, Anne-Cécile Orgerie, Théo Losekoot, Nicolas Waldburger, François Schwartzentruber, Caroline Collange
    - Absents: Martin Quinson, Yassine Hadjadj-Aoul, Matthieu Simonin, Laurent Garnier
- Simon: Résumé Archipel 

*Sujets de discussions*.
- Organiser des ateliers SEnS à l'IRISA à la rentrée (format?)
- Comment bifurquer? Quels projets?
- Essayer d'aller parler de recréer de la transdisciplinarité à l'échelle de Rennes?
- Quel statut pour ce groupe ? Quels objectifs ?
  Notamment: quel mattermost? On a hijacked le mattermost Climat, mais ce n'était pas son intention à la base.
- Quel périmètre ? éthique (lien avec les séminaires éthique de l'IRMAR) ? 
