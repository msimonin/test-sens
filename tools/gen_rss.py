import datetime as dt
from pathlib import Path

import rfeed
import pytz

from icalevents.icalevents import events

import pytz

PARIS = pytz.timezone('Europe/Paris')
_BASE_URL = "https://zimbra.inria.fr/home/matthieu.simonin@inria.fr/SEnS"
CALENDAR_HTML = f"{_BASE_URL}.html"
CALENDAR_ICS = f"{_BASE_URL}.ics"

start = dt.datetime.fromisoformat("2022-09-01")
end = dt.datetime.today() + dt.timedelta(days=7)

items = []
for e in events("https://zimbra.inria.fr/home/matthieu.simonin@inria.fr/SEnS.ics", tzinfo=PARIS, start=start, end=end, sort=True):
    items.append(
        rfeed.Item(
            title=f"[{e.start.strftime('%a. %d %B %y')}] e.summary",
            link=CALENDAR_HTML,
            description=f"""
# Where / Où

{e.location}

# Description

{e.description}

# Links

{CALENDAR_ICS}
{CALENDAR_HTML}
""",
            author="SeNS-Rennes",
            pubDate=e.start
        )
    )

feed = rfeed.Feed(
    title = "SeNS-Rennes feed",
    link = "https://sens-rennes.gitlabpages.inria.fr/feed.xml",
    description = "Calendar events as an rss feed",
    language = "fr-FR",
    lastBuildDate = dt.datetime.now().astimezone(PARIS),
    items = items
)


Path("feed.xml").write_text(feed.rss())

