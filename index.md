---
title: Groupe SEnS-Rennes
---

## Missions, objectifs

Le groupe Sciences, Environnement et Société de Rennes (SEnS-Rennes) vise à s’interroger sur nos thématiques de recherche au regard des enjeux climatiques actuels et futurs.

Concrètement le groupe vise à :

1. **Sensibiliser** : organiser des ateliers de sensibilisation sur les enjeux environnementaux liés au numérique (ateliers SEnS, fresques du climat, fresques du numérique, atelier prospective EcoInfo). Plusieurs formateurs des différents ateliers sont membres du labo.
2. **Se former** : organiser des séminaires-débats sur ces sujets, notamment en lien avec l’éthique scientifique, l’épistémologie, les méthodes scientifiques, la neutralité de la science, etc. Partenariat avec l’IRMAR pour leur cycle de conférences sur l’éthique scientifique. Cercle de lecture.
3. **Échanger** : organiser des discussions régulières entre les personnes du groupe sur les réorientations thématiques (comment, pourquoi, difficultés, etc.)

## Agenda partagé

Retrouver les prochains évènements dans notre [agenda partagé](https://zimbra.inria.fr/home/matthieu.simonin@inria.fr/SEnS.html)

## Autres initiatives locales reliées
- Groupe *Développement durable* de l'Inria: comment décarboner la recherche? (voyages en particulier)
- [Séminaire d'éthique](https://irmar.univ-rennes1.fr/seminars?f[0]=seminar_type%3A288): Invitation d'extérieur ou discussion en interne, séance toutes les deux semaines (IRMAR)
- [Epolar](https://epolar.hypotheses.org/): Groupe d'écologie politique à Rennes

