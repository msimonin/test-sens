
ALL=$(wildcard *.md)

all: $(ALL:.md=.html)

%.html: %.md
	pandoc --mathjax -s $< -o $@ --css style-md.css --template markdown-tpl.html
