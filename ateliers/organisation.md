# Ateliers futurs
## Atelier du 9 février
Communication: Martin
Salle: Pétri-Turing.

## Atelier du 16 janvier
Communication: Martin.
Salle: Pétri-Turing.
## Atelier du 4 novembre 2022
*Organisateurs*: Thomas, Caroline, Simon, Davide
*Salle*: Pétri-Turing

- [x] Réserver la salle
- [ ] Envoyer un email aux inscrits
- [ ] Réunion de préparation : 24 octobre
- [ ] Réimprimer les papiers consommés

# Ateliers passés
## Atelier du 12 Octobre 2022
***Organisateurs***: Simon, Martin, Caroline
***Salle***: Pétri-Turing.

Matériels:
- [x] Imprimer des exemplaires du manuel + questions -> Martin
- [x] Post-It -> Tout le monde
- [ ] Stylos, feutres, papier -> Simon
- [x] Compacter la ressource documentaire -> Simon

Préparation:
- [x] Lecture de la ressource documentaire
- [x] Réunion de préparation
- [ ] Préparer l'intro: présenter aussi le groupe SEnS-Rennes
- [x] Prévenir les inscrits
- [x] Réserver la salle
- [x] Créer l'event
- [x] Envoyer un email à tout le monde.

#### Notes sur le déroulé
*Introduction*. 
- Objectif de la journée
  - Cadre de discussion où l'on se parle sans chercher à converger vers un avis unique
  - Tout le monde en informatique fait de l'optimisation. Donc on peut dire qu'on fait de l'écologie, mais non, ce n'est pas vrai car ça continue de s'empirer
  - On est pas là ajd pour chercher une solution, mais envisager de nouvelles questions.
- Rôle de Mt: répartition du temps de parole entre les participants

*Prospectives*: 2040 -> Début de la crise à la place
