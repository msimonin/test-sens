# Déroulé de l'atelier du 16 janvier 2023

Contraintes: doit finir à trois heures.

Résumé: on rassemble les deux premières phases, et on pousse la 3e phase le matin.

## 9h-9h15: Introduction

Introduction par l'animateur; tour de table.

## 9h15-11h15: Pourquoi travaillez-vous dans la recherche scientifique

On se partage en petits groupes.

- (5minutes) Exposé des objectifs

- (5minutes) Lecture du texte d'Alexander Grothendieck

- (25minutes) Échange en petit groupe « Pourquoi travaillez-vous dans la 
  recheche ? Pourquoi cette recheche ? »

- (25minutes) On continue sur l'échange « Pourquoi restez vous dans la recherche ? ». Qu'est-ce qui vous y attache ?

- (20minutes) Chaque participant écrit 4 postits

- (40minutes) Restitition, patates, etc. (à voir en fonction de la structure)

PAUSE
# 11.15-12.30: Ressource documentaire
- (15min) positionnement sur les dix questions
- (Reste) Débat / Lecture d'un texte ?

# 12:30-13:30 déjeuner

# 13:30-14.45 Prospectives

On se partage en petit groupe

- (10minutes) Objectifs de la séance + Lecture du texte sur la prospective

- (30minutes) Chaque groupe met au point deux scénarios avec au moins un 
  scénario souhaitable.

- (35minutes) Restitution, discussion sur les scénarios

# 14.45-15h Bilan


